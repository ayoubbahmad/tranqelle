/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component, useContext} from 'react';

import InitialNavigator from './routes/InitialNavigator';
import {View, Text, TouchableOpacity} from 'react-native';
import NetworkProvider, {NetworkContext} from './context/NetworkProvider';
import {Colors} from './utils/constants/Colors';
import NoInternetComponent from './components/NoInternetComponent';

const App: () => React$Node = () => {
  // class App extends Component {
  // static contextType = NetworkContext;

  // const context = useContext(NetworkContext);

  // render() {
  // console.log(this.context);

  return (
    <NetworkProvider>
      <NetworkContext.Consumer>
        {context => {
          return context.isConnected ? (
            <InitialNavigator />
          ) : (
            <NoInternetComponent />
          );
        }}
      </NetworkContext.Consumer>
    </NetworkProvider>
  );
};

export default App;
