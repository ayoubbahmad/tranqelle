import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  Dimensions,
  StyleSheet,
} from 'react-native';
import {Colors} from '../utils/constants/Colors';

const {width} = Dimensions.get('window');

const Header = props => {
  return (
    <View style={[styles.header, props.style]}>
      {props.close ? (
        <TouchableOpacity
          style={styles.closeButton}
          onPress={() => {
            props.close();
          }}>
          <Image
            source={require('../assets/icons/close.png')}
            style={styles.closeImage}
            resizeMode={'contain'}
          />
        </TouchableOpacity>
      ) : (
        <TouchableOpacity style={styles.closeButton} onPress={() => {}}>
          <Image style={styles.closeImage} resizeMode={'contain'} />
        </TouchableOpacity>
      )}
      {props.title && <Text style={styles.headerTitle}>{props.title}</Text>}
      <TouchableOpacity
        style={styles.closeButton}
        onPress={() => {
          alert('Menu Feature! SOON!');
        }}>
        <Image
          source={require('../assets/icons/burger_menu.png')}
          style={styles.closeImage}
          resizeMode={'contain'}
        />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    width: width,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 12,
    paddingBottom: 8,
  },
  headerTitle: {
    color: Colors.PRIMARY_COLOR,
    fontSize: 24,
    fontWeight: '600',
    fontFamily: 'Titillium-Regular',
  },
  closeButton: {
    padding: 12,
  },
  closeImage: {
    width: 20,
    height: 20,
  },
});
export default Header;
