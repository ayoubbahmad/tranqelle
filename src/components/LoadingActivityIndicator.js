import React from 'react';
import {View, ActivityIndicator, StyleSheet, Dimensions} from 'react-native';
import {Colors} from '../utils/constants/Colors';
const {height, width} = Dimensions.get('window');

const LoadingActivityIndicator = props => {
  const {loading} = props;
  return (
    loading && (
      <View style={styles.activityIndicatorContainer}>
        <ActivityIndicator
          style={styles.activityIndicator}
          size={40}
          color={Colors.PRIMARY_COLOR}
        />
      </View>
    )
  );
};

const styles = StyleSheet.create({
  activityIndicatorContainer: {
    position: 'absolute',
    zIndex: 2,
    height: height,
    width: width,
    backgroundColor: `${Colors.PRIMARY_COLOR}10`,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default LoadingActivityIndicator;
