import React, {useContext} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {NetworkContext} from '../context/NetworkProvider';
import {Colors} from '../utils/constants/Colors';

const NoInternetComponent = () => {
  const networkContext = useContext(NetworkContext);
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: Colors.BACKGROUND_COLOR,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Text
        style={{
          fontSize: 16,
          color: Colors.PRIMARY_COLOR,
          fontFamily: 'Titillium-Regular',
        }}>
        No Internet connection
      </Text>
      <TouchableOpacity
        onPress={networkContext.tryAgain}
        style={{
          backgroundColor: Colors.PRIMARY_COLOR,
          paddingHorizontal: 20,
          paddingVertical: 10,
          marginTop: 20,
          borderRadius: 8,
        }}>
        <Text
          style={{
            color: 'white',
            fontFamily: 'Titillium-Regular',
            fontSize: 16,
          }}>
          Retry
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default NoInternetComponent;
