import React, {Component} from 'react';
import NetInfo from '@react-native-community/netinfo';

export const NetworkContext = React.createContext({
  isConnected: false,
  tryAgain: () => {},
});

export default class NetworkProvider extends Component {
  state = {
    isConnected: true,
  };

  componentDidMount() {
    this.unsubscribe = NetInfo.addEventListener(this.handleConnectivityChange);
  }

  componentWillUnmount() {
    // NetInfo.removeEventListener(this.handleConnectivityChange);
    this.unsubscribe();
  }

  handleConnectivityChange = state => {
    console.log('NetworkProvider class Connection type ', state.type);
    console.log('NetworkProvider class Is connected?', state.isConnected);
    // isConnected =>
    this.setState({isConnected: state.isConnected});
  };

  tryAgain = () => {
    NetInfo.fetch().then(state => {
      console.log('Connection type', state.type);
      console.log('Is connected?', state.isConnected);
      if (state.isConnected) {
        this.setState({isConnected: state.isConnected});
      }
    });
  };

  render() {
    return (
      <NetworkContext.Provider
        value={{isConnected: this.state.isConnected, tryAgain: this.tryAgain}}>
        {this.props.children}
      </NetworkContext.Provider>
    );
  }
}
