import React from 'react';
import {createStackNavigator} from 'react-navigation-stack';
import HomeScreen from '../screens/homeScreen/HomeScreen';
import {createAppContainer} from 'react-navigation';
import {NavigationRouteConfigMap} from 'react-navigation';
import AboutScreen from '../screens/aboutScreen/AboutScreen';
import TestimonyScreen from '../screens/testimonyScreen/TestimonyScreen';
import AddTestimonyScreen from '../screens/addTestimonyScreen/AddTestimonyScreen';
import SecureZoneScreen from '../screens/secureZoneScreen/SecureZoneScreen';
import TestimonyDetailsScreen from '../screens/testimonyDetailsScreen/TestimonyDetailsScreen';
import SecureZoneDetailsScreen from '../screens/secureZoneDetailsScreen/SecureZoneDetailsScreen';

const StackNavigatorConfig = {};

const RouteConfigs: NavigationRouteConfigMap = {
  HomeScreen: {
    screen: HomeScreen,
    navigationOptions: {headerShown: false},
  },
  AboutScreen: {
    screen: AboutScreen,
  },
};

const AppNavigator = createStackNavigator(
  {
    HomeScreen: {
      screen: HomeScreen,
      navigationOptions: {
        // headerShown: false,
        // title: 'TranqElle',
      },
    },
    AboutScreen: {
      screen: AboutScreen,
    },
    TestimonyScreen: {
      screen: TestimonyScreen,
    },
    AddTestimonyScreen: {
      screen: AddTestimonyScreen,
    },
    SecureZoneScreen: {
      screen: SecureZoneScreen,
    },
    TestimonyDetailsScreen: {
      screen: TestimonyDetailsScreen,
    },
    SecureZoneDetailsScreen: {
      screen: SecureZoneDetailsScreen,
    },
  },
  // RouteConfigs
  StackNavigatorConfig,
);

export default createAppContainer(AppNavigator);
