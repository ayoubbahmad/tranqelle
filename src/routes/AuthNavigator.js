import {createSwitchNavigator, createAppContainer} from 'react-navigation';
import LoginScreen from '../screens/loginScreen/LoginScreen';
import RegisterScreen from '../screens/registerScreen/RegisterScreen';

const AuthNavigator = createSwitchNavigator({
  LoginScreen: LoginScreen,
  RegisterScreen: RegisterScreen,
});

export default createAppContainer(AuthNavigator);
