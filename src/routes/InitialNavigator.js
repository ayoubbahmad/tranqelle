import {createSwitchNavigator, createAppContainer} from 'react-navigation';
import StartingScreen from '../screens/startingScreen/StartingScreen';
import SplashScreen from '../screens/splashScreen/SplashScreen';
import AppNavigator from './AppNavigator';
import AuthNavigator from './AuthNavigator';

const InitialNavigator = createSwitchNavigator({
  SplashScree: SplashScreen,
  StartingScreen: StartingScreen,
  AuthNavigator: AuthNavigator,
  AppNavigator: AppNavigator,
});

export default createAppContainer(InitialNavigator);
