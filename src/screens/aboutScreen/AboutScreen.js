import React, {Component} from 'react';
import {Text, View, StyleSheet, SafeAreaView, ScrollView} from 'react-native';
import {Colors} from '../../utils/constants/Colors';
import Screens from '../../utils/constants/Screens';

const DEFAULT_LATITUDE_DELTA = 0.009866;
const DEFAULT_LONGITUDE_DELTA = 0.0007757;

const ONE_DAY_MILLIYSECOND = 86400000;

class AboutScreen extends Component {
  static navigationOptions = {
    title: Screens.ABOUT_PAGE,
  };
  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: Colors.BACKGROUND_COLOR}}>
        <ScrollView>
          <View style={{padding: 20}}>
            <Text
              style={{
                fontFamily: 'Titillium-Bold',
                textAlign: 'center',
                padding: 20,
                // fontWeight: 'bold',
                fontSize: 30,
              }}>
              À propos
            </Text>
            <Text
              style={{
                fontSize: 18,
                fontFamily: 'Titillium-Regular',
              }}>
              {`    `}Qui est derrière l’application? Initié en 2007, le
              programme Génération A’Venir créé par la Fondation Friedrich Ebert
              en Tunisie, a pour objectif d’offrir aux jeunes actifs dans la
              société civile ou dans les partis politiques l’opportunité de
              travailler sur une thématique d’actualité dans leur société.
              {`\n\n   `}Il leur permet également de bénéficier de plusieurs
              cycles de formation visant à renforcer leurs capacités en matière
              de communication, leadership, gestion de projet et plusieurs
              autres thématiques.{`\n\n   `}Un groupe de participants a eu
              l’idée de lancer une application mobile visant à mettre l’accent
              sur l’un des sujets tabous en Tunisie. Cette application au nom de
              Yeszina a pour but la création d’une communauté jeune, dynamique
              et solidaire pour la lutte contre le harcèlement sexuel dans les
              moyens de transport public.{`\n\n   `}Après collecte des données,
              l’Agence District Digital Agency a pris en charge le développement
              de l’application, le site web ainsi que la campagne de
              médiatisation.{`\n\n   `}Comment utiliser l’application?
              L’application YesZina est disponible sur Android. {`\n\n   `}Après
              l’avoir télécharger, l’utilisateur doit créer un compte afin de se
              connecter et accéder aux services offerts tout en gardant
              l’anonymat total : {`\n   `}1. Les témoignages quotidiens:
              l’utilisateur aura accès aux témoignages dénonçant des actes de
              harcèlement sexuel dans les moyens de transport public. {`\n   `}
              2. La carte géographiques: Avec une géo-localisation instantanée,
              l’utilisateur peut localiser les cas d’abus et de harcèlement.{' '}
              {`\n\n   `}3. Badge: Afin d’engager plus de gens dans le mouvement
              contre le harcèlement sexuel dans les moyens de transport, les
              utilisateurs de l’application peuvent ajouter un badge à leurs
              photos de profil sur les réseaux sociaux. {`\n\n   `}4. Ajouter un
              témoignage: Les utilisateurs de l’application pourront ajouter des
              témoignages en précisant le moyen de transport où l’incident a eu
              lieu ainsi que le type d’agression. {`\n\n   `}Les témoignages ne
              doivent pas porter de nom ou d’information personnelle.{' '}
              {`\n\n   `}Les personnes concernées doivent certifier à la fin de
              chaque témoignage de la véracité de leurs propos avec l’endroit et
              la date de l’incident mentionnée au dessous du texte. {`\n\n   `}
              La localisation du témoignage s’ajoute automatiquement grâce à la
              liaison de l’application au réseau GPS mobile.
            </Text>
          </View>
        </ScrollView>
      </SafeAreaView>
      // <MapView
      //   showsMyLocationButton={false}
      //   provider={PROVIDER_GOOGLE}
      //   showsUserLocation
      //   ref={map => {
      //     this.map = map;
      //   }}
      //   style={{
      //     position: 'absolute',
      //     height: '100%',
      //     width: '100%',
      //     paddingBottom: 50,
      //     paddingRight: 50,
      //     paddingTop: 50,
      //   }}
      //   region={region}
      //   onRegionChange={() => {
      //     if (!regionChanged) regionChanged = true;
      //   }}
      //   onRegionChangeComplete={region => {
      //     if (regionChanged) this.mapRegionChanged(region);
      //   }}
      //   onPress={event => {
      //     console.log(event.nativeEvent.coordinate);
      //   }}
      // />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // ...StyleSheet.absoluteFillObject,
    // height: 400,
    // width: 400,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  map: {
    // ...StyleSheet.absoluteFillObject,
    flex: 1,
  },
});

export default AboutScreen;
