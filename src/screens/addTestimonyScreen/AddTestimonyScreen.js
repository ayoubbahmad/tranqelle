import React, {Component} from 'react';
import {
  ScrollView,
  SafeAreaView,
  Dimensions,
  View,
  Text,
  Image,
  Modal,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Platform,
  ActivityIndicator,
  Alert,
} from 'react-native';

import {Colors} from '../../utils/constants/Colors';
import DropDown from './components/DropDown';

import unchecked_checkbox from '../../assets/icons/unchecked_checkbox.png';
import checked_checkbox from '../../assets/icons/checked_checkbox.png';
import DropDownModal from './components/DropDownModal';
import DateModal from './components/DateModal';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import Axios from 'axios';
import Screens from '../../utils/constants/Screens';

const {height, width} = Dimensions.get('window');

const date = new Date(1598051730000);

const KEY = {
  SITUATION: 'Situation',
  GENRE_VICTIME: 'Genre_victime',
  GENRE_HARCELEUR: 'Genre_harceleur',
  MOYEN_DE_TRANSPORT: 'Moyen_de_transport',
  REFERENCE: 'Reference',
  TYPE_D_AGRESSION: 'Type_d_agression',
  DESCRIPTION: 'Description',
  DATE_D_AGRESSION: 'Date_d_agression',
  LOCALISATION: 'Localisation',
};

const onChange = (event, selectedDate) => {
  const currentDate = selectedDate || date;

  // setShow(Platform.OS === 'ios');
  // setDate(currentDate);
};

const SituationArray = ['Témoin', 'Victime'];
const CHOICES = {
  Situation: ['Témoin', 'Victime'],
  Genre_victime: ['Homme', 'Femme', 'Non précisé'],
  Genre_harceleur: ['Homme', 'Femme', 'Non précisé'],
  Moyen_de_transport: ['Rue', 'Maison', 'Transport', 'autre'],
  Type_d_agression: [
    'Harcèlement verbal',
    'Harcèlement physique',
    'Violence conjugale',
    'Violence',
    'Autre',
  ],
};

export default class AddTestimonyScreen extends Component {
  static navigationOptions = {
    title: Screens.ADD_TESTIMONY_SCREEN,
  };

  state = {
    data: {
      Situation: '',
      Genre_victime: '',
      Genre_harceleur: '',
      Moyen_de_transport: '',
      Reference: '',
      Type_d_agression: '',
      Description: '',
      Date_d_agression: '',
      Localisation: {
        latitude: 0,
        longitude: 0,
      },
    },

    placeHolder: {
      Situation: 'Je suis',
      Genre_victime: 'Genre victime',
      Genre_harceleur: 'Genre harceleur',
      Moyen_de_transport: 'Lieu',
      Reference: 'Ville',
      Type_d_agression: "Type d'agression",
      Description: 'Description de la scene',
      Date_d_agression: '',
      Localisation: null,
    },
    isNowDateSelected: false,
    visibleDropDownModal: false,
    modalData: {key: '', values: []},
    showTimePickerModal: false,
    visibleMapModal: false,
    region: {
      latitude: 29.555641,
      longitude: -8.853985,
      latitudeDelta: 14.015,
      longitudeDelta: 14.0121,
    },
    initialRegion: {
      latitude: 29.555641,
      longitude: -8.853985,
      latitudeDelta: 14.015,
      longitudeDelta: 14.0121,
    },
    markerCordinats: null,
  };

  nowAsDate = async () => {
    let date = await new Date();
    this.setState({
      isNowDateSelected: !this.state.isNowDateSelected,
      data: {
        ...this.state.data,
        Date_d_agression: this.state.isNowDateSelected
          ? ''
          : date.toISOString().split('T')[0],
      },
      showTimePickerModal: false,
    });
  };

  onPressDropDown = (key, values) => {
    this.setState({
      modalData: {key, values},
      visibleDropDownModal: true,
    });
  };

  onCloseDropDownModal = () => {
    this.setState({visibleDropDownModal: false});
  };

  onChooseItem = choosedItem => {
    let data = {...this.state.data};
    data[Object.keys(choosedItem)[0]] = Object.values(choosedItem)[0];
    this.setState({data, visibleDropDownModal: false});
  };

  onChangeTimeDate = (event, selectedDate) => {
    if (selectedDate)
      this.setState({
        isNowDateSelected: Platform.select({
          android: false,
          ios: true,
        }),
        data: {
          ...this.state.data,
          Date_d_agression: selectedDate.toISOString().split('T')[0],
        },
        showTimePickerModal: Platform.select({
          android: false,
          ios: true,
        }),
      });
    else
      this.setState({
        isNowDateSelected: false,
        showTimePickerModal: false,
      });
    // this.setState({Date_d_agression: selectedDate.toISOString().split('T')[0]});
  };

  render() {
    const {
      data,
      isNowDateSelected,
      visibleDropDownModal,
      modalData,
      placeHolder,
      showTimePickerModal,
      loading,
      visibleMapModal,
      initialRegion,
    } = this.state;
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: Colors.BACKGROUND_COLOR}}>
        <ScrollView
          style={{
            flex: 1,
            paddingHorizontal: 26,
          }}>
          <View style={{height: 16}} />
          <DropDown
            title={placeHolder.Situation}
            value={data.Situation}
            onPress={() =>
              this.onPressDropDown(KEY.SITUATION, CHOICES.Situation)
            }
          />
          <DropDown
            title={placeHolder.Genre_victime}
            value={data.Genre_victime}
            onPress={() =>
              this.onPressDropDown(KEY.GENRE_VICTIME, CHOICES.Genre_victime)
            }
          />
          <DropDown
            title={placeHolder.Genre_harceleur}
            value={data.Genre_harceleur}
            onPress={() =>
              this.onPressDropDown(KEY.GENRE_HARCELEUR, CHOICES.Genre_harceleur)
            }
          />
          <DropDown
            title={placeHolder.Moyen_de_transport}
            value={data.Moyen_de_transport}
            onPress={() =>
              this.onPressDropDown(
                KEY.MOYEN_DE_TRANSPORT,
                CHOICES.Moyen_de_transport,
              )
            }
          />
          <TextInput
            placeholder={placeHolder.Reference}
            style={{
              paddingVertical: 16,
              paddingBottom: 8,
              fontSize: 16,
              fontFamily: 'Titillium-Light',
            }}
            onChangeText={Reference => {
              this.setState({data: {...data, Reference}});
            }}
            value={data.Reference}
          />
          <View style={{height: 1, backgroundColor: Colors.PRIMARY_COLOR}} />
          <DropDown
            title={placeHolder.Type_d_agression}
            value={data.Type_d_agression}
            onPress={() =>
              this.onPressDropDown(
                KEY.TYPE_D_AGRESSION,
                CHOICES.Type_d_agression,
              )
            }
          />

          <TextInput
            placeholder={placeHolder.Description}
            multiline
            style={styles.descriptionTextInput}
            numberOfLines={4}
            value={data.Description}
            onChangeText={Description => {
              this.setState({data: {...data, Description}});
            }}
          />
          <View style={{height: 1, backgroundColor: Colors.PRIMARY_COLOR}} />
          <Text style={styles.agressionDateTitle}>
            Date de l'agression: {data.Date_d_agression}
          </Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              padding: 16,
            }}>
            <TouchableOpacity
              style={{padding: 3, flexDirection: 'row'}}
              onPress={() => {
                this.nowAsDate();
              }}>
              <Image
                source={
                  isNowDateSelected ? checked_checkbox : unchecked_checkbox
                }
                style={{height: 20, width: 20}}
              />
              <Text
                style={{
                  marginHorizontal: 8,
                  color: Colors.PRIMARY_COLOR,
                  fontSize: 16,
                  fontFamily: 'Titillium-Light',
                }}>
                à l'instant!
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{padding: 3, flexDirection: 'row'}}
              onPress={() => {
                this.setState({showTimePickerModal: true});
              }}>
              <Text
                style={{
                  marginHorizontal: 8,
                  color: Colors.PRIMARY_COLOR,
                  fontSize: 16,
                  fontFamily: 'Titillium-Bold',
                }}>
                Choisir une date
              </Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            onPress={async () => {
              await this.setState({visibleMapModal: true});
              setTimeout(() => {
                Alert.alert(
                  '',
                  `Veuillez faire glisser la carte et cliquer sur l'endroit où vous avez vu le harcèlement sexual`,
                );
              }, 350);
            }}
            activeOpacity={0.8}
            style={{
              alignSelf: 'center',
              backgroundColor: Colors.PRIMARY_COLOR,
              padding: 16,
              paddingHorizontal: 48,
              borderRadius: 40,
            }}>
            <Text style={{color: 'white', fontFamily: 'Titillium-Light'}}>
              DEFINIR L'ENDROIT
            </Text>
          </TouchableOpacity>

          <DropDownModal
            data={modalData}
            onPressClose={() => this.onCloseDropDownModal()}
            onChoose={this.onChooseItem}
            // {
            //   choosedItem => {
            //   let testData = {...data};
            //   testData[Object.keys(choosedItem)[0]] = Object.values(
            //     choosedItem,
            //   )[0];
            //   console.log(testData);
            // }}
            visible={visibleDropDownModal}
          />

          <DateModal
            onCancel={() => {
              this.setState({showTimePickerModal: false, Date_d_agression: ''});
            }}
            onConfirm={() => {
              this.setState({
                showTimePickerModal: false,
                isNowDateSelected: false,
              });
            }}
            onShow={() => {
              Platform.select({
                ios: () => {
                  return;
                },
                android: () => this.setState({showTimePickerModal: false}),
              })();
            }}
            value={data.Date_d_agression}
            showTimePickerModal={showTimePickerModal}
            onChangeTimeDate={this.onChangeTimeDate}
          />

          <Modal transparent visible={visibleMapModal} animated>
            <SafeAreaView style={{flex: 1, justifyContent: 'flex-end'}}>
              {loading && (
                <View style={styles.activityIndicatorContainer}>
                  <ActivityIndicator
                    style={styles.activityIndicator}
                    size={40}
                    color={Colors.PRIMARY_COLOR}
                  />
                </View>
              )}
              <View
                style={{
                  backgroundColor: Colors.PRIMARY_COLOR,
                  borderRadius: 8,
                }}>
                <TouchableOpacity
                  style={{padding: 20}}
                  onPress={() => {
                    this.setState({visibleMapModal: false});
                  }}>
                  <Text
                    style={{
                      fontFamily: 'Titillium-Light',
                      color: 'white',
                      fontSize: 20,
                    }}>
                    Fermer
                  </Text>
                </TouchableOpacity>
                <MapView
                  showsMyLocationButton={false}
                  provider={PROVIDER_GOOGLE}
                  showsUserLocation
                  ref={map => {
                    this.map = map;
                  }}
                  style={{
                    height: height * 0.7,
                  }}
                  initialRegion={initialRegion}
                  onPoiClick={async event => {
                    let {latitude, longitude} = event.nativeEvent.coordinate;
                    await this.setState({
                      data: {...data, Localisation: {latitude, longitude}},
                    });
                  }}
                  onPress={async event => {
                    let {latitude, longitude} = event.nativeEvent.coordinate;
                    await this.setState({
                      data: {...data, Localisation: {latitude, longitude}},
                    });
                  }}>
                  {data.Localisation ? (
                    <Marker coordinate={data.Localisation}>
                      <Image
                        resizeMode={'contain'}
                        style={{height: 30}}
                        source={require('../../assets/icons/customPicker.png')}
                      />
                    </Marker>
                  ) : null}
                </MapView>
                <TouchableOpacity
                  onPress={async () => {
                    try {
                      this.setState({loading: true});

                      const {
                        Situation,
                        Genre_victime,
                        Genre_harceleur,
                        Moyen_de_transport,
                        Reference,
                        Type_d_agression,
                        Description,
                        Date_d_agression,
                        Localisation: {latitude, longitude},
                      } = data;

                      const isAnEmptyFilds =
                        Situation === '' ||
                        Genre_victime === '' ||
                        Genre_harceleur === '' ||
                        Moyen_de_transport === '' ||
                        Reference === '' ||
                        Type_d_agression === '' ||
                        Description === '' ||
                        Date_d_agression === '' ||
                        (latitude === 0 && longitude === 0);

                      if (isAnEmptyFilds)
                        throw new Error(
                          `Vous n'avez pas entré toutes les informations nécessaires`,
                        );
                      else {
                        const {response} = await Axios.post(
                          'https://tranqelle.herokuapp.com/temoignages',
                          data,
                        );
                      }

                      await this.setState({
                        loading: false,
                        visibleMapModal: false,
                      });
                      await this.props.navigation.goBack();
                      setTimeout(() => {
                        Alert.alert(
                          'Merci.',
                          `Votre témoignage est bien enregistré\nMerci.`,
                        );
                      }, 100);
                    } catch (e) {
                      await this.setState({
                        loading: false,
                      });
                      Alert.alert('', e.message);
                    }
                  }}
                  activeOpacity={0.8}
                  style={{
                    position: 'absolute',
                    alignSelf: 'center',
                    bottom: 30,
                    backgroundColor: Colors.PRIMARY_COLOR,
                    paddingVertical: 12,
                    paddingHorizontal: 24,
                    borderRadius: 40,
                  }}>
                  <Text
                    style={{
                      fontFamily: 'Titillium-Light',
                      color: 'white',
                      fontSize: 20,
                    }}>
                    OK
                  </Text>
                </TouchableOpacity>
              </View>
            </SafeAreaView>
          </Modal>
          <View style={{height: 40}} />
        </ScrollView>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  descriptionTextInput: {
    fontFamily: 'Titillium-Light',
    backgroundColor: '#00000010',
    marginTop: 16,
    height: 120,
    textAlignVertical: 'top',
    paddingHorizontal: 16,
    paddingTop: 8,
    paddingBottom: 3,
    fontSize: 16,
  },
  agressionDateTitle: {
    fontFamily: 'Titillium-Regular',
    padding: 20,
    paddingBottom: 0,
    // textAlign: 'center',
    color: Colors.PRIMARY_COLOR,
    fontSize: 14,
    marginTop: 10,
  },
  activityIndicatorContainer: {
    position: 'absolute',
    zIndex: 2,
    height: height,
    width: width,
    backgroundColor: `${Colors.PRIMARY_COLOR}10`,
    justifyContent: 'center',
    alignItems: 'center',
  },
  activityIndicator: {
    // width: 60,
    // height: 60,
  },
});
