import React from 'react';
import {
  Text,
  Modal,
  TouchableOpacity,
  Platform,
  Dimensions,
  View,
  StyleSheet,
} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';

const {height, width} = Dimensions.get('window');
const DateModal = props => {
  return Platform.select({
    android: props.showTimePickerModal ? (
      <DateTimePicker
        testID="dateTimePicker"
        timeZoneOffsetInMinutes={0}
        value={props.value ? new Date(props.value) : new Date()}
        mode={'date'}
        is24Hour={true}
        display="calendar"
        onChange={props.onChangeTimeDate}
        maximumDate={new Date()}
        style={{backgroundColor: 'blue', flex: 1}}
      />
    ) : null,
    ios: props.showTimePickerModal ? (
      <Modal transparent style={{}}>
        <View
          style={{
            height: height,
            width: width,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#00000020',
          }}>
          <View
            style={{
              backgroundColor: 'white',
              width: width * 0.9,
              borderRadius: 16,
              overflow: 'hidden',
            }}>
            <DateTimePicker
              testID="dateTimePicker"
              timeZoneOffsetInMinutes={0}
              value={props.value ? new Date(props.value) : new Date()}
              mode={'date'}
              is24Hour={true}
              display="calendar"
              onChange={props.onChangeTimeDate}
              maximumDate={new Date()}
            />
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-end',
                padding: 16,
                alignItems: 'center',
              }}>
              <TouchableOpacity
                style={styles.button}
                onPress={() => {
                  props.onCancel();
                }}>
                <Text style={styles.buttonText}>CANCEL</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.button}
                onPress={() => {
                  props.onConfirm();
                }}>
                <Text style={styles.buttonText}>OK</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    ) : null,
  });
};

const styles = StyleSheet.create({
  button: {
    paddingHorizontal: 16,
    paddingVertical: 16,
    // backgroundColor: 'blue',
    marginHorizontal: 4,
  },
  buttonText: {
    fontFamily: 'Titillium-Light',
    fontSize: 16,
    fontWeight: '600',
  },
});

export default DateModal;
