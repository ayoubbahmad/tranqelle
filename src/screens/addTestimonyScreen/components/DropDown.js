import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {Colors} from '../../../utils/constants/Colors';

const DropDown = props => {
  return (
    <View>
      <TouchableOpacity
        style={{paddingVertical: 8, paddingBottom: 0, paddingHorizontal: 3}}
        onPress={() => {
          props.onPress();
        }}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            opacity: props.value ? 1 : 0.3,
          }}>
          <Text
            style={{
              paddingVertical: 8,
              fontSize: 16,
              fontFamily: 'Titillium-Light',
            }}>
            {props.value ? props.value : props.title}
          </Text>
          <View
            style={{
              width: 0,
              height: 0,
              backgroundColor: 'transparent',
              borderLeftWidth: 6,
              borderRightWidth: 6,
              borderTopWidth: 10,
              borderLeftColor: 'transparent',
              borderRightColor: 'transparent',
              borderBottomColor: Colors.PRIMARY_COLOR,
              marginRight: 8,
            }}
          />
        </View>
      </TouchableOpacity>

      <View style={{height: 1, backgroundColor: Colors.PRIMARY_COLOR}} />
    </View>
  );
};

export default DropDown;
