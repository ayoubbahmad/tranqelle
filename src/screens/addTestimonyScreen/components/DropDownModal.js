import React from 'react';
import {
  View,
  Text,
  Modal,
  TouchableOpacity,
  SafeAreaView,
  Dimensions,
} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';
import {Colors} from '../../../utils/constants/Colors';

const {width, height} = Dimensions.get('window');
const DropDownModal = (props) => {
  return (
    <Modal transparent visible={props.visible}>
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: '#00000040',
          justifyContent: 'center',
          alignItems: 'center',
          paddingHorizontal: 40,
        }}>
        <TouchableOpacity
          style={{
            width: width,
            height: height,
            position: 'absolute',
          }}
          onPress={() => {
            props.onPressClose();
          }}
        />
        <View
          style={{
            backgroundColor: 'white',
            width: width * 0.9,
            padding: 16,
            // maxHeight: '50%',
            borderRadius: 8,
            overflow: 'hidden',
          }}>
          <FlatList
            style={{
              borderRadius: 8,
              overflow: 'hidden',
              backgroundColor: `${Colors.PRIMARY_COLOR}20`,
            }}
            ItemSeparatorComponent={() => {
              return (
                <View
                  style={{
                    height: 1,
                    backgroundColor: Colors.PRIMARY_COLOR,
                    marginHorizontal: 3,
                    opacity: 0.4,
                  }}
                />
              );
            }}
            showsVerticalScrollIndicator={false}
            data={props.data.values}
            keyExtractor={(item) => `${item}ab`}
            renderItem={({item, index}) => {
              return (
                <View
                  key={`${item}${index}`}
                  style={
                    {
                      // backgroundColor: `${Colors.PRIMARY_COLOR}20`,
                      // borderRadius: 8,
                    }
                  }>
                  <TouchableOpacity
                    onPress={() => {
                      let toLog = {};
                      toLog[props.data.key] = item;
                      props.onChoose(toLog);
                    }}>
                    <Text
                      style={{
                        paddingVertical: 20,
                        textAlign: 'center',
                        fontWeight: '700',
                        fontSize: 16,
                        color: Colors.PRIMARY_COLOR,
                      }}>
                      {item}
                    </Text>
                  </TouchableOpacity>
                </View>
              );
            }}
          />
        </View>
      </SafeAreaView>
    </Modal>
  );
};

export default DropDownModal;
