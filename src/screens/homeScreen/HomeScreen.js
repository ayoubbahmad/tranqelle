import React, {Component} from 'react';
import {View, StyleSheet, SafeAreaView} from 'react-native';
import {Colors} from '../../utils/constants/Colors';

import TEMOIGNAGE_IMAGE from '../../assets/icons/temoignage.png';
import ADD_TEMOIGNAGE_IMAGE from '../../assets/icons/ajouter_temoignage.png';
import SECURE_ZONE_IMAGE from '../../assets/icons/secure_zone.png';
import A_PROPOS_IMAGE from '../../assets/icons/a_propos.png';

import TouchableImageBlock from './components/TouchableImageBlock';
import TouchableText from './components/TouchableText';
import Screens from '../../utils/constants/Screens';

const TEMOIGNAGE = 'Témoignage';
const AJOUTER_TEMOIGNAGE = 'Ajouter un témoignage';
const ZONE_DE_SECURITE = 'Zone de sécurité';
const A_PROPOS = 'Á propos';

export default class HomeScreen extends Component {
  static navigationOptions = {
    title: Screens.APP_NAME,
  };

  constructor(props) {
    super(props);
  }
  navigateTo = distination => {
    this.props.navigation.navigate(distination);
  };
  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: Colors.BACKGROUND_COLOR}}>
        <View style={styles.container}>
          <View style={styles.row}>
            <TouchableImageBlock
              imageSource={TEMOIGNAGE_IMAGE}
              onPress={() => {
                this.navigateTo('TestimonyScreen');
              }}
            />
            <TouchableImageBlock
              imageSource={ADD_TEMOIGNAGE_IMAGE}
              onPress={() => {
                this.navigateTo('AddTestimonyScreen');
              }}
            />
          </View>
          <View style={styles.row}>
            <TouchableText
              text={TEMOIGNAGE}
              onPress={() => {
                this.navigateTo('TestimonyScreen');
              }}
            />
            <TouchableText
              text={AJOUTER_TEMOIGNAGE}
              onPress={() => {
                this.navigateTo('AddTestimonyScreen');
              }}
            />
          </View>
          <View style={styles.row}>
            <TouchableImageBlock
              imageSource={SECURE_ZONE_IMAGE}
              onPress={() => {
                this.navigateTo('SecureZoneScreen');
              }}
            />

            <TouchableImageBlock
              imageSource={A_PROPOS_IMAGE}
              onPress={() => {
                this.navigateTo('AboutScreen');
              }}
            />
          </View>
          <View style={styles.row}>
            <TouchableText
              text={ZONE_DE_SECURITE}
              onPress={() => {
                this.navigateTo('SecureZoneScreen');
              }}
            />
            <TouchableText
              text={A_PROPOS}
              onPress={() => {
                this.navigateTo('AboutScreen');
              }}
            />
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  headerContainer: {
    // backgroundColor: Colors.BACKGROUND_COLOR,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 16,
  },
  row: {flexDirection: 'row', paddingHorizontal: 8},
});
