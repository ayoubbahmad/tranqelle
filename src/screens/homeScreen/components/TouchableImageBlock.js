import React from 'react';
import {TouchableOpacity, Image, StyleSheet} from 'react-native';

const TouchableImageBlock = props => {
  return (
    <TouchableOpacity
      style={styles.block}
      // activeOpacity={0.9}
      onPress={() => {
        props.onPress ? props.onPress() : '';
      }}>
      <Image style={styles.blockImage} source={props.imageSource} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  block: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 3,
    marginBottom: 0,
    paddingTop: 40,
    // backgroundColor: 'red',
  },
  blockImage: {height: 70, width: 70, resizeMode: 'contain'},
});

export default TouchableImageBlock;
