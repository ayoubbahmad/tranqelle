import React from 'react';
import {Text, TouchableOpacity, StyleSheet} from 'react-native';
import {Colors} from '../../../utils/constants/Colors';

const TouchableText = props => {
  return (
    <TouchableOpacity
      style={styles.blockTextContainer}
      onPress={() => {
        props.onPress ? props.onPress() : '';
      }}>
      <Text style={styles.blockText}>{props.text}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  blockTextContainer: {
    flex: 1,
    margin: 3,
    marginTop: 0,
    paddingVertical: 8,
  },
  blockText: {
    fontSize: 20,
    color: Colors.PRIMARY_COLOR,
    textAlign: 'center',
    fontFamily: 'Titillium-Bold',
  },
});

export default TouchableText;
