import React, {Component} from 'react';
import {
  Text,
  View,
  SafeAreaView,
  StyleSheet,
  Image,
  TextInput,
  Dimensions,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
  Modal,
  Alert,
} from 'react-native';
import {Colors} from '../../utils/constants/Colors';
import Axios from 'axios';

import LinearGradient from 'react-native-linear-gradient';

import PageVideo from '../startingScreen/components/PageVideo';
import AuthService from '../../services/AuthService';
import API from '../../utils/API';
import WebService from '../../utils/constants/WebService';
import StringUtilities from '../../utils/StringUtilities';

const {width, height} = Dimensions.get('window');

const authService = new AuthService();
const stringUtilities = new StringUtilities();

export default class LoginScreen extends Component {
  state = {
    email: '',
    password: '',
    loading: false,
    submited: false,
    isValidPassword: false,
    isValidEmail: false,
    isVideoMdalVisibale: false,
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({
        isVideoMdalVisibale: this.props.navigation.getParam(
          'showVideoModal',
          false,
        ),
      });
    }, 300);
  }

  onPressCreateAccount = () => {
    this.props.navigation.navigate('RegisterScreen');
  };

  onPressLoginButton = async () => {
    this.setState({loading: true});

    let isValidEmail = stringUtilities.isValidEmail(this.state.email);
    let isValidPassword = stringUtilities.isValidPassword(this.state.password);

    if (isValidEmail && isValidPassword) {
      try {
        const {email, password} = this.state;

        const {jwt, user} = await authService.login({
          identifier: email,
          password,
        });

        this.setState({
          isValidEmail: valideMail,
          isValidPassword: validPassword,
          submited: true,
          loading: false,
        });

        if (user) {
          this.props.navigation.navigate('AppNavigator');
        } else {
          // alert('');
        }
      } catch (e) {
        this.setState({
          isValidEmail: valideMail,
          isValidPassword: validPassword,
          submited: true,
          loading: false,
        });

        if (e.response.status < 500 && e.response.status >= 400) {
          Alert.alert(
            '',
            `Les informations fournies sont incorrectes, veuillez vérifier votre e-mail et votre mot de passe et réessayer`,
          );
        }
        // else {
        //   Alert.alert(
        //     '',
        //     `Nous ne pouvons pas établir cette connexion, veuillez réessayer plus tard`,
        //   );
        // }
      }
    } else {
      this.setState({
        loading: false,
        isValidEmail,
        isValidPassword,
        submited: true,
      });
    }
  };

  onPressAnnounymeLogin = () => {
    this.setState({loading: true});
    setTimeout(async () => {
      await this.setState({loading: false});
      this.props.navigation.navigate('AppNavigator');
    }, 1000);
  };

  render() {
    const {
      email,
      password,
      isValidPassword,
      isValidEmail,
      submited,
      isVideoMdalVisibale,
    } = this.state;
    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: Colors.BACKGROUND_COLOR,
        }}>
        <Modal
          transparent
          visible={isVideoMdalVisibale}
          animated
          animationType={'slide'}>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#00000080',
              paddingHorizontal: 20,
            }}>
            <TouchableOpacity
              style={{position: 'absolute', width: '100%', height: '100%'}}
              onPress={() => {
                this.setState({isVideoMdalVisibale: false});
              }}
            />
            <PageVideo
              onClose={() => {
                this.setState({isVideoMdalVisibale: false});
              }}
              style={{
                alignSelf: 'center',
                borderRadius: 20,
                overflow: 'hidden',
              }}
            />
          </View>
        </Modal>
        {this.state.loading && (
          <View style={styles.activityIndicatorContainer}>
            <ActivityIndicator
              style={styles.activityIndicator}
              size={40}
              color={Colors.PRIMARY_COLOR}
            />
          </View>
        )}
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={[
            styles.scrollView,
            {
              // backgroundColor: 'red'
            },
          ]}>
          <KeyboardAvoidingView
            behavior="position"
            enabled={Platform.OS == 'ios'}>
            <View style={styles.container}>
              <Image
                source={require('../../assets/icons/colored_icon.png')}
                style={styles.headerImage}
              />

              <TextInput
                placeholder={'E-mail'}
                style={[
                  styles.textInput,
                  {
                    backgroundColor:
                      submited && !isValidEmail
                        ? `${Colors.PRIMARY_COLOR}15`
                        : 'transparent',
                  },
                ]}
                textContentType={'emailAddress'}
                keyboardType={'email-address'}
                onChangeText={email => {
                  this.setState({email});
                }}
                value={email}
              />
              <TextInput
                placeholder={'Mot de passe'}
                secureTextEntry
                style={[
                  styles.textInput,
                  {
                    backgroundColor:
                      submited && !isValidPassword
                        ? `${Colors.PRIMARY_COLOR}15`
                        : 'transparent',
                  },
                ]}
                onChangeText={password => {
                  this.setState({password});
                }}
                value={password}
              />
            </View>
          </KeyboardAvoidingView>
          <View style={styles.buttonsContainer}>
            <TouchableOpacity
              onPress={() => {
                this.onPressLoginButton();
              }}
              style={[styles.button, {backgroundColor: '#a10a4e'}]}>
              <Text style={[styles.buttonText, {color: 'white'}]}>
                DEMARRER
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={this.onPressAnnounymeLogin}
              style={[
                styles.button,
                {backgroundColor: 'white', borderWidth: 1},
              ]}>
              <Text
                style={[
                  styles.buttonText,
                  {color: '#a10a4e', textAlign: 'center', fontSize: 18},
                ]}>
                CONNEXION ANNOUNYME
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.textButton}
              onPress={() => {
                this.onPressCreateAccount();
              }}>
              <Text
                style={[
                  styles.buttonText,
                  {
                    color: '#a10a4e',
                    textAlign: 'center',
                    fontFamily: 'Titillium-Regular',
                  },
                ]}>
                Mot de passe oublié?{'\n'}Créer votre compte
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    padding: 40,
    justifyContent: 'space-between',
    // backgroundColor: 'red',
  },
  scrollView: {
    flex: 1,
    // backgroundColor: 'green',
  },
  buttonsContainer: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'flex-end',
    // backgroundColor: 'blue',
  },
  headerImage: {
    flex: 1,
    resizeMode: 'contain',
    marginBottom: 24,
  },
  textInput: {
    fontFamily: 'Titillium-Light',
    borderRadius: 1,
    borderColor: 'black',
    borderBottomWidth: 1,
    padding: 8,
    width: width * 0.8,
    fontSize: 20,
    margin: 4,
    borderTopLeftRadius: 3,
  },
  button: {
    width: width * 0.8,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 16,
    borderRadius: 32,
    margin: 8,
  },
  buttonText: {
    fontFamily: 'Titillium-Light',
    fontSize: 20,
  },
  textButton: {
    opacity: 0.7,
    paddingVertical: 16,
  },
  activityIndicatorContainer: {
    position: 'absolute',
    zIndex: 2,
    height: height,
    width: width,
    backgroundColor: '#ffffff19',
    justifyContent: 'center',
    alignItems: 'center',
  },
  activityIndicator: {
    // width: 60,
    // height: 60,
  },
});
