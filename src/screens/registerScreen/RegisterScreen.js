import React, {Component} from 'react';
import {
  Text,
  View,
  SafeAreaView,
  StyleSheet,
  Image,
  TextInput,
  Dimensions,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {Colors} from '../../utils/constants/Colors';
import Axios from 'axios';
import StringUtilities from '../../utils/StringUtilities';
import AuthService from '../../services/AuthService';

const {width, height} = Dimensions.get('window');

const stringUtilities = new StringUtilities();

const authService = new AuthService();
export default class RegisterScreen extends Component {
  state = {
    formData: {
      username: '',
      email: '',
      age: '',
      phone: '',
      gender: '',
      password: '',
      passwordConfirmation: '',
    },
    loading: false,
    submited: false,
    isValidPassword: false,
    isValidRepeatedPassword: false,
    isValidEmail: false,
  };

  constructor(props) {
    super(props);
  }

  onPressGoToLoginButton = () => {
    this.props.navigation.navigate('LoginScreen');
  };

  onPressRegisterButton = async () => {
    this.setState({loading: true});

    const {
      formData: {email, age, gender, phone, password, passwordConfirmation},
    } = this.state;

    const valideMail = stringUtilities.isValidEmail(email);
    const validPassword = stringUtilities.isValidPassword(password);
    const validRepeatedPassword = password === passwordConfirmation;

    const valideAge = stringUtilities.isValideAge(age);
    const validPhone = stringUtilities.isValidPhoneNumber(phone);

    if (
      valideMail &&
      validPassword &&
      validRepeatedPassword &&
      valideAge &&
      validPhone
    )
      try {
        const {formData} = this.state;

        const {jwt, user} = await authService.register(formData);

        // TODO: chack valide inputs for all registration data
        this.setState({
          isValidEmail: valideMail,
          isValidPassword: validPassword,
          isValidRepeatedPassword: validRepeatedPassword,
          submited: true,
          loading: false,
        });

        if (user) {
          this.props.navigation.navigate('AppNavigator');
        } else {
        }
      } catch (e) {
        this.setState({
          isValidEmail: valideMail,
          isValidPassword: validPassword,
          isValidRepeatedPassword: validRepeatedPassword,
          submited: true,
          loading: false,
        });
        alert(e);
      }
    else
      this.setState({
        isValidEmail: valideMail,
        isValidPassword: validPassword,
        isValidRepeatedPassword: validRepeatedPassword,
        submited: true,
        loading: false,
      });
  };

  render() {
    const {
      submited,
      formData,
      formData: {
        username,
        email,
        age,
        gender,
        phone,
        password,
        passwordConfirmation,
      },
    } = this.state;
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: Colors.BACKGROUND_COLOR}}>
        {this.state.loading && (
          <View style={styles.activityIndicatorContainer}>
            <ActivityIndicator
              style={styles.activityIndicator}
              size={40}
              color={Colors.PRIMARY_COLOR}
            />
          </View>
        )}
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={[styles.scrollView]}>
          <KeyboardAvoidingView
            behavior="position"
            enabled={Platform.OS == 'ios'}>
            <View style={styles.container}>
              <Image
                source={require('../../assets/icons/colored_icon.png')}
                style={styles.headerImage}
              />

              <TextInput
                placeholder={"Nom d'utilisateur"}
                style={[
                  styles.textInput,
                  {
                    backgroundColor:
                      submited && !(username.length > 3)
                        ? `${Colors.PRIMARY_COLOR}15`
                        : 'transparent',
                  },
                ]}
                onChangeText={username => {
                  this.setState({formData: {...formData, username}});
                }}
                value={username}
              />

              <TextInput
                placeholder={'E-mail'}
                style={[
                  styles.textInput,
                  {
                    backgroundColor:
                      submited && !stringUtilities.isValidEmail(email)
                        ? `${Colors.PRIMARY_COLOR}15`
                        : 'transparent',
                  },
                ]}
                textContentType={'emailAddress'}
                keyboardType={'email-address'}
                onChangeText={email => {
                  this.setState({formData: {...formData, email}});
                }}
                value={email}
              />

              <TextInput
                placeholder={'Age'}
                style={[
                  styles.textInput,
                  {
                    backgroundColor:
                      submited && !stringUtilities.isValideAge(age)
                        ? `${Colors.PRIMARY_COLOR}15`
                        : 'transparent',
                  },
                ]}
                keyboardType={'numeric'}
                onChangeText={age => {
                  this.setState({formData: {...formData, age}});
                }}
                value={`${age}`}
              />

              <TextInput
                placeholder={'Genre'}
                style={[
                  styles.textInput,
                  {
                    backgroundColor:
                      submited && !stringUtilities.isValidEmail(gender)
                        ? `${Colors.PRIMARY_COLOR}15`
                        : 'transparent',
                  },
                ]}
                onChangeText={gender => {
                  this.setState({formData: {...formData, gender}});
                }}
                value={gender}
              />

              <TextInput
                placeholder={'Numéro de téléphone'}
                style={[
                  styles.textInput,
                  {
                    backgroundColor:
                      submited && !stringUtilities.isValidPhoneNumber(phone)
                        ? `${Colors.PRIMARY_COLOR}15`
                        : 'transparent',
                  },
                ]}
                textContentType={'telephoneNumber'}
                keyboardType={'phone-pad'}
                onChangeText={phone => {
                  this.setState({formData: {...formData, phone}});
                }}
                value={phone}
              />

              <TextInput
                placeholder={'Mot de passe'}
                secureTextEntry
                style={[
                  styles.textInput,
                  {
                    backgroundColor:
                      submited && !stringUtilities.isValidPassword(password)
                        ? `${Colors.PRIMARY_COLOR}15`
                        : 'transparent',
                  },
                ]}
                onChangeText={password => {
                  this.setState({formData: {...formData, password}});
                }}
                value={password}
              />
              <TextInput
                placeholder={'Répéter le mot de passe'}
                secureTextEntry
                style={[
                  styles.textInput,
                  {
                    backgroundColor:
                      submited &&
                      (!stringUtilities.isValidPassword(password) ||
                        !(passwordConfirmation === password))
                        ? `${Colors.PRIMARY_COLOR}15`
                        : 'transparent',
                  },
                ]}
                onChangeText={passwordConfirmation => {
                  this.setState({
                    formData: {...formData, passwordConfirmation},
                  });

                  this.setState({});
                }}
                value={passwordConfirmation}
              />
            </View>
          </KeyboardAvoidingView>
          <View style={styles.buttonsContainer}>
            <TouchableOpacity
              onPress={() => {
                this.onPressRegisterButton();
              }}
              style={[styles.button, {backgroundColor: '#a10a4e'}]}>
              <Text style={[styles.buttonText, {color: 'white'}]}>
                S'INSCRIRE
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.textButton}
              onPress={() => {
                this.onPressGoToLoginButton();
              }}>
              <Text
                style={[
                  styles.buttonText,
                  {color: '#a10a4e', textAlign: 'center'},
                ]}>
                Déjà enregistré?{'\n'}
                Connecte-toi maintenant
                {/* Mot de passe oublié?Créer votre compte */}
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    padding: 40,
    justifyContent: 'space-between',
  },
  scrollView: {
    flex: 1,
  },
  buttonsContainer: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'flex-end',
  },
  headerImage: {
    flex: 1,
    resizeMode: 'contain',
    marginBottom: 24,
  },
  textInput: {
    fontFamily: 'Titillium-Light',
    borderRadius: 1,
    borderColor: 'black',
    borderBottomWidth: 1,
    padding: 8,
    width: width * 0.8,
    fontSize: 20,
    margin: 4,
    borderTopLeftRadius: 3,
  },
  button: {
    width: width * 0.8,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 16,
    borderRadius: 32,
  },
  buttonText: {
    fontFamily: 'Titillium-Light',
    fontSize: 20,
  },
  textButton: {
    opacity: 0.7,
    paddingVertical: 16,
  },
  activityIndicatorContainer: {
    position: 'absolute',
    zIndex: 2,
    height: height,
    width: width,
    backgroundColor: '#ffffff19',
    justifyContent: 'center',
    alignItems: 'center',
  },
  activityIndicator: {
    // width: 60,
    // height: 60,
  },
});
