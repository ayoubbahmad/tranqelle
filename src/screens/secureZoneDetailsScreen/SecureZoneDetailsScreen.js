import React, {Component} from 'react';
import {
  Text,
  View,
  SafeAreaView,
  StyleSheet,
  ActivityIndicator,
  Dimensions,
} from 'react-native';
import {Colors} from '../../utils/constants/Colors';
import {ScrollView} from 'react-native-gesture-handler';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import Screens from '../../utils/constants/Screens';

const {height, width} = Dimensions.get('window');

const DEFAULT_LATITUDE_DELTA = 1.0;
const DEFAULT_LONGITUDE_DELTA = 1.0;

const TITLE = {
  ADDRESS: 'Adress: ',
  PHONE: 'Phone: ',
  EMAIL: 'Email: ',
};

export default class SecureZoneDetailsScreen extends Component {
  static navigationOptions = {
    title: Screens.SECURE_ZONE_DETAILS,
  };
  state = {
    data: {
      Name: ' ',
      City: '',
      Phone: '',
      Adress: '',
      Email: '',
    },
    loading: true,
  };
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    setTimeout(() => {
      this.setState({
        data: this.props.navigation.getParam('item', 'NO-ID'),
        loading: false,
      });
    }, 600);
  }

  render() {
    const {data, loading} = this.state;
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: Colors.BACKGROUND_COLOR}}>
        {loading ? (
          <View style={styles.activityIndicatorContainer}>
            <ActivityIndicator
              style={styles.activityIndicator}
              size={40}
              color={Colors.PRIMARY_COLOR}
            />
          </View>
        ) : (
          <ScrollView style={{flex: 1}}>
            <View
              style={{
                alignItems: 'center',
                paddingTop: 20,
                paddingHorizontal: 32,
              }}>
              <Text
                style={{
                  fontFamily: 'Titillium-Bold',
                  paddingTop: 30,
                  fontSize: 25,
                  color: Colors.PRIMARY_COLOR,
                }}>
                {data.Name}
              </Text>
              <View style={{flex: 1, paddingTop: 30, width: '100%'}}>
                <Text style={[styles.textContainer, styles.title]}>
                  {TITLE.ADDRESS}
                </Text>
                <Text
                  style={[
                    styles.textContainer,
                    {fontSize: 24, textAlign: 'center'},
                  ]}>
                  {data.Address}
                  {`\n`}
                </Text>
                <Text style={[styles.textContainer, styles.title]}>
                  {TITLE.PHONE}
                </Text>
                <Text
                  selectable
                  style={[
                    styles.textContainer,
                    {fontSize: 24, textAlign: 'center'},
                  ]}>
                  {data.Phone}
                </Text>
                <Text style={[styles.textContainer, styles.title]}>
                  {TITLE.EMAIL}
                </Text>
                <Text
                  style={[
                    styles.textContainer,
                    {fontSize: 24, textAlign: 'center'},
                  ]}>
                  {data.Email}
                </Text>
              </View>
            </View>
          </ScrollView>
        )}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  activityIndicatorContainer: {
    position: 'absolute',
    zIndex: 2,
    height: height,
    width: width,
    backgroundColor: `${Colors.PRIMARY_COLOR}10`,
    justifyContent: 'center',
    alignItems: 'center',
  },
  activityIndicator: {},
  title: {
    paddingBottom: 8,
    fontFamily: 'Titillium-Bold',
  },
  textContainer: {
    fontSize: 20,
    fontFamily: 'Titillium-Light',
    // paddingBottom: 20,
  },
});
