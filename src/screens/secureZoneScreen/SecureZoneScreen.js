import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  SafeAreaView,
  FlatList,
  StyleSheet,
  ActivityIndicator,
  Dimensions,
  Image,
} from 'react-native';
import Axios from 'axios';
import {Colors} from '../../utils/constants/Colors';

import LogoTranqelle from '../../assets/icons/LogoTranqelle.png';
import Screens from '../../utils/constants/Screens';

const {height, width} = Dimensions.get('window');

ItemSecureZone = props => {
  const {item, onPress} = props;
  return (
    <TouchableOpacity
      style={[styles.item, {padding: 20, width: '100%'}]}
      onPress={onPress}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          flex: 1,
        }}>
        <Image
          style={{height: 90, width: 90, resizeMode: 'contain'}}
          source={LogoTranqelle}
        />
        <View
          style={{
            flex: 1,
            height: '100%',
          }}>
          <Text
            style={{
              // fontWeight: 'bold',
              fontFamily: 'Titillium-Bold',
              fontSize: 20,
              color: Colors.PRIMARY_COLOR,
            }}>
            {item.Name}
          </Text>
          <Text style={{fontFamily: 'Titillium-Light'}}>
            <Text style={{fontFamily: 'Titillium-Bold'}}>Ville: </Text>
            {item.City}
          </Text>
          <Text style={{fontFamily: 'Titillium-Light'}} numberOfLines={3}>
            <Text style={{fontFamily: 'Titillium-Bold'}}>Phone: </Text>
            {item.Phone}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

class SecureZoneScreen extends Component {
  static navigationOptions = {
    title: Screens.SECURE_ZONE,
  };
  state = {
    data: [],
    loading: true,
  };
  async componentDidMount() {
    try {
      const result = await Axios.get(
        'https://tranqelle.herokuapp.com/associations',
      );
      await this.setState({data: result.data, loading: false});
    } catch (e) {
      alert(e);
    }
  }

  render() {
    const {data, loading} = this.state;
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: Colors.BACKGROUND_COLOR}}>
        {loading && (
          <View style={styles.activityIndicatorContainer}>
            <ActivityIndicator
              style={styles.activityIndicator}
              size={40}
              color={Colors.PRIMARY_COLOR}
            />
          </View>
        )}
        <FlatList
          ListHeaderComponent={() => {
            return <View style={{height: 16}} />;
          }}
          ListFooterComponent={() => {
            return <View style={{height: 16}} />;
          }}
          style={styles.flatList}
          ItemSeparatorComponent={() => {
            return (
              <View
                style={{
                  height: 1,
                  backgroundColor: Colors.PRIMARY_COLOR,
                  marginHorizontal: 20,
                }}
              />
            );
          }}
          data={data}
          renderItem={({item, index}) => {
            return (
              <ItemSecureZone
                item={item}
                onPress={() => {
                  this.props.navigation.navigate('SecureZoneDetailsScreen', {
                    item: item,
                  });
                }}
              />
            );
          }}
          keyExtractor={item => {
            return item._id;
          }}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  flatList: {flex: 1},
  item: {},
  activityIndicatorContainer: {
    position: 'absolute',
    zIndex: 2,
    height: height,
    width: width,
    backgroundColor: `${Colors.PRIMARY_COLOR}10`,
    justifyContent: 'center',
    alignItems: 'center',
  },
  activityIndicator: {},
});

export default SecureZoneScreen;
