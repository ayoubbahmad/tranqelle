import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  Dimensions,
  SafeAreaView,
  StatusBar,
} from 'react-native';
import {Colors} from '../../utils/constants/Colors';
import AsyncStorage from '@react-native-community/async-storage';
// import NetInfo from '@react-native-community/netinfo';

const {width} = Dimensions.get('window');

// const unsubscribe = NetInfo.addEventListener(state => {
//   console.log('Connection type', state.type);
//   console.log('Is connected?', state.isConnected);
// });

export default class SplashScreen extends Component {
  loadData = async () => {
    return new Promise(resolve =>
      setTimeout(() => {
        resolve('result');
      }, 2000),
    );
  };

  async componentDidMount() {
    const data = await this.loadData();
    if (data !== null) {
      AsyncStorage.getItem('isFirstTimeDone', (error, result) => {
        if (result) {
          this.props.navigation.navigate('LoginScreen', {showVideoModal: true});
        } else {
          this.props.navigation.navigate('StartingScreen');
        }
      });
    }
  }

  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: Colors.BACKGROUND_COLOR}}>
        <StatusBar hidden />
        <View style={styles.container}>
          <Image
            style={[styles.image, {width: width, height: width * 0.5}]}
            source={require('../../assets/icons/colored_icon.png')}
          />
          <View style={{alignItems: 'center'}}>
            <Text style={styles.powredBy}>Powred by</Text>
            <Image
              style={[styles.image, {height: width * 0.2}]}
              source={require('../../assets/icons/Logo_Friedrich.png')}
            />
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    padding: 44,
    justifyContent: 'space-between',
  },
  image: {
    width: width * 0.8,
    resizeMode: 'contain',
  },
  powredBy: {
    fontFamily: 'Titillium-Light',
    padding: 10,
    fontSize: 20,
    opacity: 0.2,
  },
});
