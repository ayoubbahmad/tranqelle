import React, {Component} from 'react';
import {SafeAreaView, StatusBar} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import Header from '../../components/Header';
import {Colors} from '../../utils/constants/Colors';
import PageOne from './components/PageOne';
import PageOneFr from './components/PageOneFr';
import Swiper from './components/Swiper';

export default class StartingScreen extends Component {
  state = {
    pageIndex: 0,
  };

  constructor(props) {
    super(props);
  }

  close = () => {
    AsyncStorage.setItem('isFirstTimeDone', 'true', () => {
      this.props.navigation.navigate('LoginScreen', {showVideoModal: true});
    });
  };

  performNextPage = (isLastPage) => {
    this.setState({pageIndex: this.state.pageIndex + 1});
  };

  render() {
    return (
      <>
        <StatusBar hidden />
        <SafeAreaView
          style={{flex: 1, backgroundColor: Colors.BACKGROUND_COLOR}}>
          <Header close={this.close} navigation={this.props.navigation} />
          <Swiper navigation={this.props.navigation} close={this.close}>
            <PageOne />
            <PageOneFr />
          </Swiper>
        </SafeAreaView>
      </>
    );
  }
}
