// import React from 'react';
// import {View, Image, StyleSheet, TouchableOpacity} from 'react-native';

// const Footer = (props) => {
//   return (
//     <View style={styles.footerContainer}>
//       <View style={styles.pagesIndicator}>
//         <Image
//           source={require('../../../assets/icons/active_page.png')}
//           style={styles.pageIndicatorImage}
//         />
//         <Image
//           source={require('../../../assets/icons/unactive_page.png')}
//           style={styles.pageIndicatorImage}
//         />
//       </View>
//       <TouchableOpacity
//         style={styles.nextButton}
//         onPress={() => {
//           props.performNextPage(props.isLastPage);
//         }}>
//         <Image
//           source={require('../../../assets/icons/next_icon.png')}
//           style={styles.nextImage}
//         />
//       </TouchableOpacity>
//     </View>
//   );
// };

// const styles = StyleSheet.create({
//   footerContainer: {
//     alignItems: 'flex-end',
//     padding: 8,
//     justifyContent: 'center',
//   },
//   nextImage: {height: 32, resizeMode: 'contain'},
//   nextButton: {padding: 12},
//   pagesIndicator: {
//     position: 'absolute',
//     alignSelf: 'center',
//     flexDirection: 'row',
//   },
//   pageIndicatorImage: {
//     height: 16,
//     resizeMode: 'contain',
//   },
// });

// export default Footer;
