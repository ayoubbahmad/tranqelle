import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Image,
  ScrollView,
} from 'react-native';

const {width} = Dimensions.get('window');

const PageOne = () => {
  return (
    <View style={styles.container}>
      <Image
        source={require('../../../assets/icons/colored_icon.png')}
        style={styles.image}
      />

      <ScrollView showsVerticalScrollIndicator={false}>
        <Text style={[styles.title]}>Tranqelle : تعرفي على</Text>
        <Text style={styles.text}>
          ختي كما بغا يكون عمرك او الوسط ديالك اجي تعرفي على حقك.{`\n`}كلنا
          متضامنين ضد التحرش الجنسي و اللفظي سواء فالقرى او المدن ,مكان العمل,
          وسائل النقل العمومي و حتى فالشارع.{`\n`}في تاريخ 22/02/2018 المشرع
          المغربي اصدر قانون 103-13 سعيا للقضاء على العنف ضد المرأة سوى كان لفضي
          او جسدي.{`\n`}تطبيق Tranqelle هي من ابتكار مجموعة من الشباب المغربي
          المنخرط بالتعاون مع مؤسسة فريدريش أبارت اللي وضعوا رهنه اشارتكم نافدة
          تقدر تساعدكم في الحصول على حقكم و انصافكم وحمايتكم من هاد الظاهرة.
          {`\n`}
          من خلال هاد التطبيق كنوفرو ليك المساعدة اللازمة باش déclarer ou
          déposer un témoignage حيت الهدف ديالنا هده الممارسات الي القانون
          المغربي كيجرمها و كيعاقب عليها.
        </Text>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: width,
    alignItems: 'center',
    paddingHorizontal: 24,
    flex: 1,
  },
  image: {
    marginBottom: 32,
  },
  title: {
    // fontWeight: 'bold',
    marginBottom: 16,
    width: '100%',
    fontSize: 20,
    textAlign: 'center',
    fontFamily: 'Titillium-Regular',
  },
  text: {
    textAlign: 'right',
    fontSize: 18,
    fontFamily: 'Titillium-Light',
    lineHeight: 20,
  },
});

export default PageOne;
