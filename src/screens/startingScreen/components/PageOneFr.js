import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Image,
  ScrollView,
} from 'react-native';

const {width} = Dimensions.get('window');

const PageOne = () => {
  return (
    <View style={styles.container}>
      <Image
        source={require('../../../assets/icons/colored_icon.png')}
        style={styles.image}
      />

      <ScrollView style={{width: '100%'}} showsVerticalScrollIndicator={false}>
        <Text style={[styles.title, {textAlign: 'left'}]}>
          Tranqelle a ta dispositionpeu importe le lieu ou le moment en un seul
          clic
        </Text>
        <Text style={[styles.text]}>
          Avec tranqelle, l’objectif et de créer une communauté dynamique et
          solidaire pour lutter contre ce fléau peu importe le lieu où tu es et
          à quel moment, on proposants aux victimes un outils entièrement sur et
          pratique afin de signaler le problème directement aux organismes
          prenantes.
        </Text>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: width,
    alignItems: 'center',
    paddingHorizontal: 24,
    flex: 1,
  },
  image: {
    marginBottom: 32,
  },
  title: {
    fontFamily: 'Titillium-Semibold',
    marginBottom: 16,
    width: '100%',
    fontSize: 18,
  },
  text: {fontSize: 16, fontFamily: 'Titillium-Regular', lineHeight: 22},
});

export default PageOne;
