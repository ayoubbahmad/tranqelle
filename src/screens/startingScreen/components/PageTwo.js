import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';

const {width} = Dimensions.get('window');

class PageTwo extends Component {
  state = {
    paused: true,
  };
  render() {
    return (
      <View style={styles.container}>
        <Image
          source={require('../../../assets/icons/arabic_icon.png')}
          style={styles.image}
          resizeMode={'contain'}
        />

        <ScrollView
          style={{width: '100%'}}
          showsVerticalScrollIndicator={false}>
          <Text style={[styles.title, {textAlign: 'center'}]}>
            ماتخافيش{`\n`} ماتسكتيش
          </Text>
          <Text
            style={[
              styles.text,
              {
                // fontWeight: '100',
                paddingTop: 20,
                textAlign: 'center',
              },
            ]}>
            بارطاجي... هدري... ماتخافيش... {`\n`}حنا معاك و شهادتك مهم لينا
          </Text>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: width,
    alignItems: 'center',
    paddingHorizontal: 24,
    flex: 1,
  },
  image: {
    marginBottom: 0,
    resizeMode: 'contain',
    width: width * 0.7,
  },
  title: {
    fontFamily: 'Titillium-Bold',
    lineHeight: 36,
    marginBottom: 16,
    width: '100%',
    fontSize: 28,
  },

  text: {fontSize: 24, fontFamily: 'Titillium-Light'},
});

export default PageTwo;
