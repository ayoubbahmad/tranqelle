import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import Video from 'react-native-video';
import {Colors} from '../../../utils/constants/Colors';
import LinearGradient from 'react-native-linear-gradient';

const {width} = Dimensions.get('window');

const videoUrl =
  'https://adiyat-al-muslim.s3.eu-west-3.amazonaws.com/Tranqelle+_+%D9%85%D8%A7%D8%AA%D8%AE%D8%A7%D9%81%D9%8A%D8%B4+%D9%83%D9%84%D9%86%D8%A7+%D9%85%D8%B9%D8%A7%D9%83.mp4';

class PageVideo extends Component {
  state = {
    paused: false,
    readyForDisplay: false,
    muted: false,
  };

  render() {
    const {readyForDisplay, muted} = this.state;

    return (
      <View style={[styles.container, {...this.props.style}]}>
        <LinearGradient
          colors={[Colors.PRIMARY_COLOR, Colors.PRIMARY_COLOR, 'white']}
          style={{
            height: 100,
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <TouchableOpacity
            style={{padding: 20, paddingTop: 16}}
            onPress={() => {
              this.props.onClose();
            }}>
            <Image
              style={{height: 16, width: 16}}
              source={require('../../../assets/icons/whiteCloseIcon.png')}
            />
          </TouchableOpacity>

          <TouchableOpacity
            style={{padding: 20, paddingTop: 16}}
            onPress={() => {
              this.setState({muted: !muted});
            }}>
            {muted ? (
              <Image
                style={{height: 20, width: 20}}
                source={require('../../../assets/icons/unmuted.png')}
              />
            ) : (
              <Image
                style={{height: 20, width: 20}}
                source={require('../../../assets/icons/muted.png')}
              />
            )}
          </TouchableOpacity>
        </LinearGradient>
        <TouchableOpacity
          onPress={() => {
            this.setState({paused: !this.state.paused});
          }}
          activeOpacity={1}
          style={{
            width: '100%',
            // paddingHorizontal: 20,
            justifyContent: 'center',
            backgroundColor: 'white',
          }}>
          <Video
            muted={muted}
            posterResizeMode={'contain'}
            playInBackground={false}
            repeat={false}
            paused={this.state.paused}
            source={{uri: videoUrl}} // Can be a URL or a local file.
            ref={(reference) => {
              this.player = reference;
            }}
            resizeMode={'contain'}
            style={{
              width: '100%',
              height: width * 0.5,
              backgroundColor: readyForDisplay ? 'transparent' : 'white',
            }}
            onReadyForDisplay={() => {
              this.setState({readyForDisplay: true, paused: false});
            }}
            onEnd={() => {
              this.setState({readyForDisplay: true, paused: true});
            }}
          />
          {!readyForDisplay && !this.state.paused && (
            <ActivityIndicator
              style={{position: 'absolute', alignSelf: 'center'}}
              size={40}
              color={Colors.PRIMARY_COLOR}
            />
          )}
          {this.state.paused && (
            <View
              style={{
                position: 'absolute',
                alignSelf: 'center',
                width: '100%',
                height: '100%',
                backgroundColor: readyForDisplay ? 'transparent' : 'white',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              {this.state.paused && (
                <View
                  style={{
                    height: 60,
                    width: 60,
                    backgroundColor: `${Colors.PRIMARY_COLOR}30`,
                    borderRadius: 120,
                    borderWidth: 2,
                    borderColor: Colors.PRIMARY_COLOR,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      color: Colors.PRIMARY_COLOR,
                      fontFamily: 'Titillium-Light',
                    }}>
                    Play
                  </Text>
                </View>
              )}
            </View>
          )}
        </TouchableOpacity>
        <LinearGradient
          colors={[
            'white',
            Colors.PRIMARY_COLOR,
            Colors.PRIMARY_COLOR,
            Colors.PRIMARY_COLOR,
          ]}
          style={{width: '100%', paddingTop: 60, paddingBottom: 30}}>
          <Text style={[styles.title, {textAlign: 'center'}]}>
            ماتخافيش ماتسكتيش
          </Text>
          <Text
            style={[
              styles.text,
              {
                paddingTop: 20,
                textAlign: 'center',
              },
            ]}>
            بارطاجي... هدري... ماتخافيش... {`\n`}حنا معاك و شهادتك مهم لينا
          </Text>
        </LinearGradient>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    alignItems: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  image: {
    marginBottom: 0,
    resizeMode: 'contain',
    width: '70%',
  },
  title: {
    // fontWeight: 'bold',
    fontFamily: 'Titillium-Bold',
    marginTop: 10,
    width: '100%',
    fontSize: 20,
    color: 'white',
    marginTop: -10,
  },

  text: {fontSize: 16, fontFamily: 'Titillium-Light', color: 'white'},
});

export default PageVideo;
