import React, {Component} from 'react';
import {
  Text,
  View,
  SafeAreaView,
  StyleSheet,
  ActivityIndicator,
  Dimensions,
  Image,
} from 'react-native';
import {Colors} from '../../utils/constants/Colors';
import {ScrollView} from 'react-native-gesture-handler';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import Screens from '../../utils/constants/Screens';

const {height, width} = Dimensions.get('window');

const DEFAULT_LATITUDE_DELTA = 1.0;
const DEFAULT_LONGITUDE_DELTA = 1.0;

const TITLE = {
  MOYEN_DE_TRANSPORT: 'Moyen de transport:',
  REFERENCE: 'Numéro de référence:',
  TYPE_D_AGRESSION: 'Type de harcelement:',
  GENRE_VICTIME: 'Sexe victime:',
  GENRE_HARCELEUR: 'Sexe harceleur:',
  DATE_D_AGRESSION: 'Date de harcelement:',
  DESCRIPTION: 'Témoignage:',
};

export default class TestimonyDetailsScreen extends Component {
  static navigationOptions = {
    title: Screens.TESTIMONY_DETAILS_SCREEN,
  };
  state = {
    data: {
      Situation: '',
      Genre_victime: '',
      Genre_harceleur: '',
      Moyen_de_transport: '',
      Reference: '',
      Type_d_agression: '',
      Description: '',
      Date_d_agression: '',
      Localisation: {
        latitude: 0,
        longitude: 0,
      },
    },
    loading: true,
    tracksViewChanges: true,
  };
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    setTimeout(() => {
      this.setState({
        data: this.props.navigation.getParam('item', 'NO-ID'),
        loading: false,
      });
    }, 600);
  }

  render() {
    const {data, loading, tracksViewChanges} = this.state;
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: Colors.BACKGROUND_COLOR}}>
        {loading ? (
          <View style={styles.activityIndicatorContainer}>
            <ActivityIndicator
              style={styles.activityIndicator}
              size={40}
              color={Colors.PRIMARY_COLOR}
            />
          </View>
        ) : (
          <ScrollView style={{flex: 1}}>
            <View
              style={{
                alignItems: 'center',
                paddingTop: 20,
                paddingHorizontal: 32,
              }}>
              <View
                style={{
                  borderRadius: 8,
                  overflow: 'hidden',
                  width: '100%',
                  borderWidth: 1,
                  borderColor: `${Colors.PRIMARY_COLOR}40`,
                }}>
                <MapView
                  showsMyLocationButton={false}
                  provider={PROVIDER_GOOGLE}
                  showsUserLocation
                  ref={map => {
                    this.map = map;
                  }}
                  style={{
                    height: 160,
                    width: '100%',
                  }}
                  initialRegion={{
                    ...data.Localisation,
                    longitudeDelta: DEFAULT_LONGITUDE_DELTA,
                    latitudeDelta: DEFAULT_LONGITUDE_DELTA,
                  }}>
                  {data.Localisation ? (
                    <Marker
                      tracksViewChanges={tracksViewChanges}
                      coordinate={data.Localisation}>
                      <Image
                        fadeDuration={0}
                        onLoad={() => {
                          this.setState({tracksViewChanges: false});
                        }}
                        resizeMode={'contain'}
                        style={{height: 30}}
                        source={require('../../assets/icons/customPicker.png')}
                      />
                    </Marker>
                  ) : null}
                </MapView>
              </View>
              <View style={{flex: 1, paddingTop: 30, width: '100%'}}>
                <Text style={styles.textContainer}>
                  <Text style={styles.title}>{TITLE.MOYEN_DE_TRANSPORT} </Text>
                  {data.Moyen_de_transport}
                </Text>

                <Text style={styles.textContainer}>
                  <Text style={styles.title}>{TITLE.REFERENCE} </Text>
                  {data.Reference}
                </Text>

                <Text style={styles.textContainer}>
                  <Text style={styles.title}>{TITLE.TYPE_D_AGRESSION} </Text>
                  {data.Type_d_agression}
                </Text>

                <Text style={styles.textContainer}>
                  <Text style={styles.title}>{TITLE.GENRE_VICTIME} </Text>
                  {data.Genre_victime}
                </Text>

                <Text style={styles.textContainer}>
                  <Text style={styles.title}>{TITLE.GENRE_HARCELEUR} </Text>
                  {data.Genre_harceleur}
                </Text>

                <Text style={styles.textContainer}>
                  <Text style={styles.title}>{TITLE.DATE_D_AGRESSION} </Text>
                  {data.Date_d_agression}
                </Text>

                <Text style={styles.textContainer}>
                  <Text style={[styles.title]}>{TITLE.DESCRIPTION}</Text>
                  {`\n`}
                  {`\n`}
                  {data.Description}
                </Text>
              </View>
            </View>
          </ScrollView>
        )}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  activityIndicatorContainer: {
    position: 'absolute',
    zIndex: 2,
    height: height,
    width: width,
    backgroundColor: `${Colors.PRIMARY_COLOR}10`,
    justifyContent: 'center',
    alignItems: 'center',
  },
  activityIndicator: {},
  title: {fontFamily: 'Titillium-Bold'},
  textContainer: {
    fontFamily: 'Titillium-Light',
    lineHeight: 20,
    fontSize: 16,
    paddingBottom: 8,
  },
});
