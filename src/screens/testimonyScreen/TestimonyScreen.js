import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Image,
} from 'react-native';

import {Colors} from '../../utils/constants/Colors';
import Screens from '../../utils/constants/Screens';
import TestemonyService from '../../services/TestemonyService';
import TestemoniesList from './components/TestemoniesList';
import LoadingActivityIndicator from '../../components/LoadingActivityIndicator';
import TestemoniesMap from './components/TestemoniesMap';

const testemonyService = new TestemonyService();

export default class TestimonyScreen extends Component {
  static navigationOptions = {
    title: Screens.TESTIMONY_SCREEN,
  };

  state = {
    data: [],
    loading: true,
  };

  async UNSAFE_componentWillMount() {
    try {
      const data = await testemonyService.getAllTestemonies();

      await this.setState({data, loading: false});
    } catch (e) {
      alert(e);
    }
  }

  goToTestemonyDetails = item => {
    this.props.navigation.navigate('TestimonyDetailsScreen', {
      item: item,
    });
  };
  goToAddTestemonyScreen = () => {
    this.props.navigation.navigate('AddTestimonyScreen');
  };

  render() {
    const {data, loading} = this.state;
    return (
      <SafeAreaView style={styles.safeArea}>
        {loading ? (
          <LoadingActivityIndicator loading={loading} />
        ) : (
          <>
            <TestemoniesMap
              style={{flex: 1}}
              data={data}
              // navigation={this.props.navigation}
              goToTestemonyDetails={this.goToTestemonyDetails}
            />
            <TestemoniesList
              data={data}
              navigation={this.props.navigation}
              goToTestemonyDetails={this.goToTestemonyDetails}
            />
            <TouchableOpacity
              onPress={this.goToAddTestemonyScreen}
              style={{
                position: 'absolute',
                right: 10,
                bottom: 10,
                padding: 10,
              }}>
              <Image
                resizeMode={'cover'}
                style={{height: 50, width: 50}}
                source={require('../../assets/icons/addTestemonyFromMap.png')}
              />
            </TouchableOpacity>
          </>
        )}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  safeArea: {flex: 1, backgroundColor: Colors.BACKGROUND_COLOR},
});
