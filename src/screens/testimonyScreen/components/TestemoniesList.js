import React from 'react';
import {View, FlatList} from 'react-native';
import Item from './TestemonyItem';
import {Colors} from '../../../utils/constants/Colors';

const TestemoniesList = props => {
  const {data, navigation, style, goToTestemonyDetails} = props;
  return (
    <FlatList
      style={[{flex: 1}, style]}
      ListHeaderComponent={() => {
        return <View style={{height: 16}} />;
      }}
      ListFooterComponent={() => {
        return <View style={{height: 16}} />;
      }}
      ItemSeparatorComponent={() => {
        return (
          <View
            style={{
              height: 1,
              backgroundColor: Colors.PRIMARY_COLOR,
              marginHorizontal: 20,
            }}
          />
        );
      }}
      data={data}
      renderItem={({item, index}) => {
        return (
          <Item
            item={item}
            onPress={() => {
              goToTestemonyDetails(item);
            }}
          />
        );
      }}
      keyExtractor={item => {
        return item._id;
      }}
    />
  );
};

export default TestemoniesList;
