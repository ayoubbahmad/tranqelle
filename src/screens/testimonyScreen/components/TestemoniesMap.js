import React, {useState} from 'react';
import {View, Text, Image} from 'react-native';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import {Colors} from '../../../utils/constants/Colors';

const DEFAULT_LOCALIZATION = {
  latitude: 30.617213,
  longitude: -9.349248,
  longitudeDelta: 15.0,
  latitudeDelta: 15.0,
};

const TestemoniesMap = props => {
  const {style, data, goToTestemonyDetails} = props;
  const [tracksViewChanges, setTracksViewChanges] = useState(true);
  return (
    <View
      style={[
        style,
        {
          margin: 10,
          marginBottom: 4,
          borderRadius: 16,
          overflow: 'hidden',
          borderColor: Colors.PRIMARY_COLOR,
          borderWidth: 1,
        },
      ]}>
      <MapView
        showsMyLocationButton
        provider={PROVIDER_GOOGLE}
        showsUserLocation
        ref={map => {
          this.map = map;
        }}
        style={{
          flex: 1,
          // height: '100%',
          // width: '100%',
          borderRadius: 16,

          // padding: 10,
        }}
        initialRegion={DEFAULT_LOCALIZATION}>
        {data.map(testemony => {
          return (
            testemony.Localisation && (
              <Marker
                tracksViewChanges={tracksViewChanges}
                key={testemony._id}
                coordinate={testemony.Localisation}
                onPress={() => {
                  goToTestemonyDetails(testemony);
                }}>
                <Image
                  fadeDuration={0}
                  onLoad={() => {
                    setTracksViewChanges(false);
                  }}
                  resizeMode={'contain'}
                  style={{height: 30}}
                  source={require('../../../assets/icons/customPicker.png')}
                />
              </Marker>
            )
          );
        })}
      </MapView>
      {/* <Text
        style={{
          height: 50,
          width: 50,
          position: 'absolute',
          right: 20,
          bottom: 20,
          backgroundColor: 'red',
        }}>
        hello
      </Text> */}
    </View>
  );
};

export default TestemoniesMap;
