import React, {Component} from 'react';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {View, Image, Text, StyleSheet, Dimensions} from 'react-native';
import {Colors} from '../../../utils/constants/Colors';

import manImage from '../../../assets/icons/man.png';
import womanImage from '../../../assets/icons/woman.png';
const {height, width} = Dimensions.get('window');

export default props => {
  const {item, onPress} = props;
  return (
    <TouchableOpacity
      style={[styles.item, {padding: 20, width: '100%'}]}
      onPress={onPress}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          flex: 1,
        }}>
        <Image
          style={{height: 90, width: 90, resizeMode: 'contain'}}
          source={item.Genre_victime == 'Homme' ? manImage : womanImage}
        />
        <View
          style={{
            flex: 1,
            height: '100%',
          }}>
          <Text style={{fontFamily: 'Titillium-Light'}}>
            <Text style={{fontFamily: 'Titillium-Bold'}}>
              Type d'agression:{' '}
            </Text>
            {item.Type_d_agression}
          </Text>
          <Text style={{fontFamily: 'Titillium-Light'}}>
            <Text style={{fontFamily: 'Titillium-Bold'}}>Date: </Text>
            {item.Date_d_agression}
          </Text>
          <Text style={{fontFamily: 'Titillium-Light'}} numberOfLines={3}>
            {item.Description}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  flatList: {flex: 1},
  item: {},
  activityIndicatorContainer: {
    position: 'absolute',
    zIndex: 2,
    height: height,
    width: width,
    backgroundColor: `${Colors.PRIMARY_COLOR}10`,
    justifyContent: 'center',
    alignItems: 'center',
  },
  activityIndicator: {},
});
