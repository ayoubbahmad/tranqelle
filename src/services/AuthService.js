import API from '../utils/API';
import WebService from '../utils/constants/WebService';

export default class AuthService {
  login = async ({identifier, password}) => {
    try {
      const {data} = await API.post(WebService.WS_USER_LOGIN, {
        identifier,
        password,
      });
      if (data) return data;
    } catch (e) {
      throw e;
    }
  };

  register = async formData => {
    const newFormData = {
      ...formData,
      firstLastName: formData.username,
      username: formData.email,
    };

    try {
      const {data} = await API.post(WebService.WS_USER_REGISTER, newFormData);
      if (data) return data;
    } catch (e) {
      throw e;
    }
  };
}
