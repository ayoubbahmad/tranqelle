import API from '../utils/API';
import WebService from '../utils/constants/WebService';

export default class TestemonyService {
  getAllTestemonies = async () => {
    try {
      const {data} = await API.get(WebService.WS_GET_TESTEMONIES);
      if (data) return data;
    } catch (e) {
      throw e;
    }
  };
}
