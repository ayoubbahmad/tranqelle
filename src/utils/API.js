import axios from 'axios';
import {AxiosConfigs} from './constants/Configs';

const API = axios.create({
  baseURL: AxiosConfigs.BASE_URL,
  timeout: AxiosConfigs.TIMEOUT,
});

API.interceptors.response.use(
  (res) => res,
  (err) => {
    // switch (err.response.status) {
    //   case 503:
    //     console.log('interceptors 503');
    //     break;
    //   case 404:
    //     console.log('interceptors 404');
    //     break;
    //   case 400:
    //     console.log('interceptors 400');
    //     break;
    //   case 403:
    //     console.log('interceptors 403');
    //     break;
    //   default:
    //     break;
    // }
    throw err;
  },
);

export default API;
