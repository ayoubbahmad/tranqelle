export default class StringUtilities {
  isValidEmail = email => {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
      return true;
    }
    return false;
  };

  isValidPassword = password => {
    if (password.length >= 6) {
      return true;
    }
    return false;
  };

  isValidPhoneNumber = phone => {
    if (
      /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im.test(phone)
    ) {
      return true;
    }
    return false;
  };

  isValideAge = (age: Number) => {
    return (
      parseInt(age, 10) > 15 && parseInt(age, 10) < 150 && /^^\d+$/.test(age)
    );
  };
}
