const Screens = {
  APP_NAME: 'Tranqelle',
  ABOUT_PAGE: 'Á Propos',
  TESTIMONY_SCREEN: 'Témoignages',
  TESTIMONY_DETAILS_SCREEN: 'Details de témoignage',
  ADD_TESTIMONY_SCREEN: 'Ajouter un témoignage',
  SECURE_ZONE: 'Zone de sécurité',
  SECURE_ZONE_DETAILS: 'Details',
};

export default Screens;
