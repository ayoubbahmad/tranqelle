const WebService = {
  WS_USER_LOGIN: '/auth/local',
  WS_USER_REGISTER: '/auth/local/register',
  WS_GET_TESTEMONIES: '/temoignages',
};

export default WebService;
